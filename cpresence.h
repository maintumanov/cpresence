#ifndef CPRESENCE_H
#define CPRESENCE_H

#include <QObject>
#include <QProcess>
#include <QTcpSocket>
#include <QFile>
#include <QDir>
#include <QTimer>

struct user {
    QString mac;
    QString ip;
    QString name;
};

class cpresence : public QObject
{
    Q_OBJECT
public:
    explicit cpresence(QObject *parent = Q_NULLPTR);
    QString routerIP;
    QString routerLogin;
    QString routerPassword;
    bool debug;
    void begin();

public slots:
    void check();
    void terminalRead();

private:
    QTcpSocket terminal;
    QList<QString> listMAC;
    QList<QString> lastMAC;
    QList<user> users;
    QTimer timer;
    int state;


    void loadUsers();
    void cleanLine(QByteArray *data);
    void fillMAC(QString data);
    void compareMAC();
    bool existLastMAC(QString MAC);
    bool existNewMAC(QString MAC);
    void cmdprocessed(QString cmd);
    void blockWorkStation(QString ip);
};

#endif // CPRESENCE_H
