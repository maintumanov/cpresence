
[Setup]
AppId={{228F6294-B721-4424-983A-2D39C03B19B9}
AppName=cPresence
AppVersion=0.1.2.0
DefaultDirName={pf}\SignalNet\cpresence
DefaultGroupName=SignalNet
;UninstallDisplayIcon=favicon.ico
;SetupIconFile=favicon.ico
Compression=lzma2
SolidCompression=yes
OutputDir=..\!Distrib
OutputBaseFilename=SetupCPresence 0.1.2.0
AllowNoIcons=yes
;AppPublisherURL=http://signalnet.com
AppPublisher=SignalNet, LLC.
CreateUninstallRegKey=yes 

[Languages]
Name: "ru"; MessagesFile: "compiler:Languages\Russian.isl"

[Files]
Source: "..\build-cPresence-Desktop_Qt_5_10_1_MinGW_32bit-Release\release\cpresence.exe"; DestDir: "{app}"

Source: "C:\Qt\5.10.1\mingw53_32\bin\libgcc_s_dw2-1.dll"; DestDir: "{app}"
Source: "C:\Qt\5.10.1\mingw53_32\bin\libstdc++-6.dll"; DestDir: "{app}"
Source: "C:\Qt\5.10.1\mingw53_32\bin\libwinpthread-1.dll"; DestDir: "{app}"
Source: "C:\Qt\5.10.1\mingw53_32\bin\Qt5Core.dll"; DestDir: "{app}"
Source: "C:\Qt\5.10.1\mingw53_32\bin\Qt5Network.dll"; DestDir: "{app}"

[Icons]
Name: "{group}\cPresence"; Filename: "{app}\cPresence.exe"
