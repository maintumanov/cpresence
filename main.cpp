#include <QCoreApplication>
#include <qcommandlineparser.h>
#include <qcommandlineoption.h>
#include <QTextCodec>
#include <QTranslator>
#include <QObject>
#include <QSettings>

#include "cpresence.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QTextCodec * codec  = QTextCodec::codecForName("utf-8");
    // QTextCodec *codec = QTextCodec::codecForName("cp1251");
    QTextCodec::setCodecForLocale(codec);
    QCoreApplication::setOrganizationName("SignalNet");
    QCoreApplication::setApplicationName("cPresence");
    QCoreApplication::setApplicationVersion(QLatin1String("0.1.2.0"));

    QCommandLineParser parser;
    parser.setApplicationDescription("TCP Repiter");
    parser.addHelpOption();
    parser.addVersionOption();
    cpresence presence;

        presence.routerIP = "192.168.130.1";
        presence.routerLogin = "admin";
        presence.routerPassword = "pas$worD";
        presence.debug = true;

    QCommandLineOption targetDirectoryOptionA(QStringList() << "a" << "address",
                                              QCoreApplication::translate("main", "Sets the connection address router."),
                                              QCoreApplication::translate("main", "Router IP address"));
    parser.addOption(targetDirectoryOptionA);


    QCommandLineOption targetDirectoryOptionL(QStringList() << "l" << "login",
                                              QCoreApplication::translate("main", "Specify the login of the router."),
                                              QCoreApplication::translate("main", "Login"));
    parser.addOption(targetDirectoryOptionL);

    QCommandLineOption targetDirectoryOptionP(QStringList() << "p" << "password",
                                              QCoreApplication::translate("main", "Specify the password of the router."),
                                              QCoreApplication::translate("main", "Password"));
    parser.addOption(targetDirectoryOptionP);

    QCommandLineOption targetDirectoryOptionD(QStringList() << "d" << "debug",
                                              QCoreApplication::translate("main", "Debug."),
                                              QCoreApplication::translate("main", "Debug"));
    parser.addOption(targetDirectoryOptionD);

    parser.process(a);

    if (parser.isSet(targetDirectoryOptionA)) presence.routerIP = parser.value(targetDirectoryOptionA);
    if (parser.isSet(targetDirectoryOptionL)) presence.routerLogin = parser.value(targetDirectoryOptionL);
    if (parser.isSet(targetDirectoryOptionP)) presence.routerPassword = parser.value(targetDirectoryOptionP);
    if (parser.isSet(targetDirectoryOptionD)) presence.debug = true;

    QSettings settings;

    if (!presence.routerIP.isEmpty() && !presence.routerLogin.isEmpty() && !presence.routerPassword.isEmpty()) {
        presence.begin();
    } else qDebug() << "error settings";



    return a.exec();
}
